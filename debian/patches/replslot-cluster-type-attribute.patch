From 578dc392918b098d68ebf00d50023b635db7e139 Mon Sep 17 00:00:00 2001
From: Michael Banck <michael.banck@credativ.de>
Date: Tue, 10 Dec 2024 11:55:59 +0100
Subject: [PATCH] Add optional 'cluster_type' attribute to permanent
 replication slots. (#3229)

This allows to set whether a particular permanent replication slot should always be created ('cluster_type=any', the default), or just on a primary ('cluster_type=primary') or standby ('cluster_type=standby') cluster, respectively.
---
 docs/dynamic_configuration.rst | 3 ++-
 patroni/dcs/__init__.py        | 4 +++-
 2 files changed, 5 insertions(+), 2 deletions(-)

diff --git a/docs/dynamic_configuration.rst b/docs/dynamic_configuration.rst
index 09413312d..908bc3f68 100644
--- a/docs/dynamic_configuration.rst
+++ b/docs/dynamic_configuration.rst
@@ -61,9 +61,10 @@ In order to change the dynamic configuration you can use either :ref:`patronictl
 
    -  **my\_slot\_name**: the name of the permanent replication slot. If the permanent slot name matches with the name of the current node it will not be created on this node. If you add a permanent physical replication slot which name matches the name of a Patroni member, Patroni will ensure that the slot that was created is not removed even if the corresponding member becomes unresponsive, situation which would normally result in the slot's removal by Patroni. Although this can be useful in some situations, such as when you want replication slots used by members to persist during temporary failures or when importing existing members to a new Patroni cluster (see :ref:`Convert a Standalone to a Patroni Cluster <existing_data>` for details), caution should be exercised by the operator that these clashes in names are not persisted in the DCS, when the slot is no longer required, due to its effect on normal functioning of Patroni.
 
-      -  **type**: slot type. Could be ``physical`` or ``logical``. If the slot is logical, you have to additionally define ``database`` and ``plugin``.
+      -  **type**: slot type. Could be ``physical`` or ``logical``. If the slot is logical, you have to additionally define ``database`` and ``plugin``. If the slot is physical, you can optionally define ``cluster_type``.
       -  **database**: the database name where logical slots should be created.
       -  **plugin**: the plugin name for the logical slot.
+      -  **cluster_type**: the type of cluster (``primary`` or ``standby``) the slot should only be created on, otherwise it will not be created or an already existing slot will be dropped.
 
 -  **ignore\_slots**: list of sets of replication slot properties for which Patroni should ignore matching slots. This configuration/feature/etc. is useful when some replication slots are managed outside of Patroni. Any subset of matching properties will cause a slot to be ignored.
 
diff --git a/patroni/dcs/__init__.py b/patroni/dcs/__init__.py
index 4a3516426..19d3c1aa1 100644
--- a/patroni/dcs/__init__.py
+++ b/patroni/dcs/__init__.py
@@ -998,7 +998,9 @@ def __permanent_slots(self) -> Dict[str, Union[Dict[str, Any], Any]]:
     @property
     def permanent_physical_slots(self) -> Dict[str, Any]:
         """Dictionary of permanent ``physical`` replication slots."""
-        return {name: value for name, value in self.__permanent_slots.items() if self.is_physical_slot(value)}
+        return {name: value for name, value in self.__permanent_slots.items() if self.is_physical_slot(value)
+                and ((global_config.is_standby_cluster and value.get('cluster_type') != 'primary')
+                or (not global_config.is_standby_cluster and value.get('cluster_type') != 'standby'))}
 
     @property
     def __permanent_logical_slots(self) -> Dict[str, Any]:
